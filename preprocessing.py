#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 03:00:00 2018

@author: xiaolin
"""
import numpy as np

def prepare_seq2seq_data(dataset, look_back, look_ahead):
    dataX, dataY = [], []
    for i in range(len(dataset) - look_back - look_ahead +1):
        input_seq = dataset[i:(i + look_back)]
        output_seq = dataset[i + look_back:(i + look_back + look_ahead)]
        dataX.append(input_seq)
        dataY.append(output_seq)
    dataX = np.reshape(np.array(dataX),[-1,look_back,1])
    dataY = np.reshape(np.array(dataY),[-1,look_ahead,1])
    return dataX, dataY

class Preprocess(object):
    def __init__(self, train, validation, test, look_back, look_ahead):
        self.mean_x = np.mean(train[:,0])
        self.std_x = np.std(train[:,0])
        self.test_labels = test[:,1]
        self.train = (train[:,0] - self.mean_x)/self.std_x
        self.validation = (validation[:,0] - self.mean_x)/self.std_x
        self.test = (test[:,0] - self.mean_x)/self.std_x
        self.look_back = look_back
        self.look_ahead = look_ahead
    
    def process(self):
        X_train, y_train = prepare_seq2seq_data(self.train, self.look_back, self.look_ahead)
        X_validation, y_validation =  prepare_seq2seq_data(self.validation, self.look_back, self.look_ahead)
        X_test, y_test = prepare_seq2seq_data(self.test, self.look_back, self.look_ahead)
        X_test_labels,y_test_labels = prepare_seq2seq_data(self.test_labels, self.look_back, self.look_ahead)

        # 1 step look ahead
        y_train = y_train.reshape(len(y_train),1)
        y_validation = y_validation.reshape(len(y_validation),1)
        y_test = y_test.reshape(len(y_test),1)
        
        return X_train, y_train, X_validation, y_validation, X_test, y_test, y_test_labels



#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 15:09:27 2018

@author: xiaolin
"""

import numpy as np
import pandas as pd

from sklearn.base import TransformerMixin
from sklearn.pipeline import make_pipeline


df = pd.read_csv("resources/data/nab/nab_machine_temperature/NAB_machine_temperature_system_failure.csv")

class SetColAsIndex(TransformerMixin):
    def __init__(self, col):
        self.col = col

    def transform(self, X, **transform_params):
        X.index = X.loc[:, self.col].apply(lambda x: pd.to_datetime(x))
        return X

    def fit(self, X, y=None, **fit_params):
        return self
    
class SortTimeSeries(TransformerMixin):
    def transform(self, X, **transform_params):
        X = X.sort_index()
        return X

    def fit(self, X, y=None, **fit_params):
        return self
    
process_pipeline = make_pipeline(SetColAsIndex('timestamp'),
                                SortTimeSeries())
df = process_pipeline.fit_transform(df)
df = df.drop('timestamp', 1)


df['anomaly'] = 0
anomalies = ['2013-12-11 06:00:00', '2013-12-16 17:25:00', '2014-01-28 13:55:00', '2014-02-08 14:30:00']
for anomaly_date in anomalies:
    df.set_value(anomaly_date, 'anomaly', 1)
    

df_test = df['2014-01-25':'2014-02-19']
df_train = df['2013-12-20':'2014-01-16']
df_validation = df['2014-01-17':'2014-01-24']

df_dict = {}
df_dict['train'] = df_train
df_dict['test'] = df_test
df_dict['validation'] = df_validation

count = 0
for key in df_dict:
    print "Size of %s: %d"%(key,df_dict[key].shape[0])
    count += df_dict[key].shape[0]
print "Total: %d"%(count)

for key in df_dict:
    np.save("resources/data/nab/nab_machine_temperature/%s"%(key),df_dict[key])



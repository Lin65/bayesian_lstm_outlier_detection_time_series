#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 17:43:33 2018

@author: xiaolin
"""

import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style("whitegrid")

def prediction_plot(pred_res, truth, true_anormalies, name):
    pred_mean = np.mean(pred_res, axis = 0)
    pred_std = np.std(pred_res, axis = 0)

    plt.figure(figsize = (20, 7))
    lower1 = pred_mean - 2*pred_std
    upper1 = pred_mean + 2*pred_std 
    
    lower2 = pred_mean - 3*pred_std
    upper2 = pred_mean + 3*pred_std
    
    plt.fill_between(range(len(truth)), lower1, upper1, color ='k', alpha =0.4, label ='95% PI')
    plt.fill_between(range(len(truth)), lower2, upper2, color ='k', alpha =0.2, label ='99.7% PI')
    
    plt.plot(truth, 'b', label='Truth')
    
    for i in range(len(true_anormalies)):
        if i == 0:
            plt.plot(true_anormalies[i], truth[true_anormalies[i]], 'ro', markersize = 20, alpha =0.4, label ='Anormaly')
        plt.plot(true_anormalies[i], truth[true_anormalies[i]], 'ro', markersize = 20, alpha =0.4)

    plt.xlabel('Time step',fontsize=20)
    plt.ylabel('Temperature',fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(bbox_to_anchor=(1, 0),loc="lower right", borderaxespad=0.,frameon=True,prop={'size': 20})
    plt.savefig(name,format='pdf', dpi=300)

    
#true_anormalies = np.where(y_test_labels==1)[0]
#prediction_plot(pred_test, y_test, true_anormalies, name = 'test_dataset_02.pdf')
class Metric_Error_distribution(object):
    def __init__(self, y, y_pred_point, y_test,y_test_labels, pred_test_point, alpha=0.95):
        #
        # y and y_pred_point can be validation data or training data, they are used to compute error distribution
        #
        self.y_test = y_test
        self.pred_test_point = pred_test_point
        
        error = y - y_pred_point
        self.endpoints = norm.interval(alpha, loc = np.mean(error), scale = np.std(error))  
        
        error_test = (y_test - pred_test_point).reshape(len(y_test), )
        self.outliers_test = np.where((error_test <self.endpoints[0]) | (error_test >self.endpoints[1]))[0]
        self.true_anormalies_test = np.where(y_test_labels==1)[0]
        
    def res_plot(self, ylabel1, name):
        
        plt.figure(figsize=(20,10))
        plt.subplots_adjust(hspace=0.1)
        ax1 = plt.subplot(211)
        ax1.plot(self.y_test, label='Truth')
        ax1.plot(self.pred_test_point,ls ='dashed',label='Prediction',color ='y')

        for loc in self.outliers_test:
            ax1.axvline(x=loc,color='r',alpha=.2)

        for anormaly in self.true_anormalies_test:
            ax1.plot(anormaly, self.y_test[anormaly], 'k.', markersize=20.0, alpha=0.4)

        plt.ylabel(ylabel1,fontsize=20)
        plt.yticks(fontsize=20)
        plt.legend(bbox_to_anchor=(0, 0),loc="lower left", borderaxespad=0.,frameon=True,prop={'size': 20})


        ax2 = plt.subplot(212, sharex=ax1)
        ax2.plot(self.y_test - self.pred_test_point,label='Error', color ='r')
        ax2.axhline(y=self.endpoints[0],ls='dashed',label='Boundries')
        ax2.axhline(y=self.endpoints[1],ls='dashed')
        plt.legend(bbox_to_anchor=(0, 0),loc="lower left", borderaxespad=0.,frameon=True,prop={'size': 20})

        xticklabels = ax1.get_xticklabels()
        plt.setp(xticklabels, visible=False)
        plt.xlabel("Time step",fontsize=20)
        plt.ylabel("Error",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        
        plt.savefig(name,format='pdf', dpi=300)
        
        
class Metric_prediction_distribution(object):
    def __init__(self, y, y_pred_distribution, y_test,y_test_labels, pred_test, alpha=0.95, mode = 'combination'):
        
        self.y_test = y_test
        # compute p value then use distribution of p value to compute threshold
        mean_pred = np.mean(y_pred_distribution, axis = 0)
        std_pred = np.std(y_pred_distribution, axis = 0)
        self.std_max = std_pred.max()
        
        p_value = []
        for i in range(len(y)):
            cdf = norm.cdf(y[i], loc = mean_pred[i], scale = std_pred[i])
            p_value.append(min(cdf, 1-cdf))

        p_value = np.array(p_value).reshape(len(p_value),) 
        self.p_value_max = p_value.max()
        
        p_value_II = std_pred/self.std_max - p_value/self.p_value_max
        _,self.threshold = norm.interval(alpha, loc = p_value_II.mean(), scale = p_value_II.std())
        
        p_value_std = std_pred/self.std_max
        _,self.threshold_std = norm.interval(alpha, loc = p_value_std.mean(), scale = p_value_std.std())
        
        self.mean_pred_test = np.mean(pred_test,axis=0)
        self.std_pred_test = np.std(pred_test, axis=0)

        self.outliers_test = []
        self.mode = mode
        self.p_value_test = []

        for i in range(len(y_test)):
            cdf = norm.cdf(y_test[i], loc = self.mean_pred_test[i], scale = self.std_pred_test[i])
            self.p_value_test.append(min(cdf, 1-cdf))

        self.p_value_test= np.array(self.p_value_test).reshape(len(self.p_value_test),)
        self.p_value_II_test = self.std_pred_test/self.std_max - np.array(self.p_value_test)/self.p_value_max

        for i in range(len(y_test)):            
            if self.mode =='std':
                if self.std_pred_test[i] > self.threshold_std:
                    self.outliers_test.append(i)
            else:
                if self.p_value_II_test[i] > self.threshold:
                    self.outliers_test.append(i)
   
        self.true_anormalies_test = np.where(y_test_labels==1)[0]
        
    def res_plot(self, ylabel1, name):
        plt.figure(figsize=(20,10))
        plt.subplots_adjust(hspace=0.1)
        ax1 = plt.subplot(211)
        ax1.plot(self.y_test, label='Truth')
        for loc in self.outliers_test:
            ax1.axvline(x=loc,color='r',alpha=0.1)

        for anormaly in self.true_anormalies_test:
            ax1.plot(anormaly, self.y_test[anormaly], 'k.', markersize=30.0, alpha=0.4)

        plt.ylabel(ylabel1, fontsize=20)
        plt.yticks(fontsize=20)
        plt.legend(bbox_to_anchor=(1, 1),loc="upper right", borderaxespad=0.,frameon=True,prop={'size': 20})

        ax2 = plt.subplot(212)
        ax2.plot(self.std_pred_test/self.std_max, label = 'STD', color = 'g')
        ax2.plot(-self.p_value_test, label = '- P-Value', color = 'y')
        ax2.plot(self.p_value_II, label = 'STD - P-Value', color = 'b')

        ax2.axhline(y=self.threshold,ls='dashed', color = 'b')
        ax2.axhline(y=self.threshold_std,ls='dashed', color = 'g')

        plt.legend(bbox_to_anchor=(1, 1),loc="upper right", borderaxespad=0.,frameon=True,prop={'size': 20})

        xticklabels = ax1.get_xticklabels()
        plt.setp(xticklabels, visible=False)

        plt.xlabel("Time step",fontsize=20)
        plt.ylabel("Metrics",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)

        plt.savefig(name,format='pdf', dpi=300)
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    
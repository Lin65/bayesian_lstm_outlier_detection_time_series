## Bayesian LSTM for outlier detection in time series

This repository contains code for outlier detection in time series. Two kinds of LSTM based detectors are implemented. 

## Dependencies

This software is dependent on the following packages: numpy, scipy, sklearn, keras, pandas

## Datasets

Two data sets are tested:

1. Machine temperature (can be downloaded from http://www.cs.ucr.edu/~eamonn/discords/ )
2. Power demand (can be downloaded from https://github.com/numenta/NAB/tree/master/data)

## Results
1. Bayesian LSTM outlier detector on machine temperature dataset:
![Machine temperature](fig/MC_dropout_results_02.pdf)

2. Bayesian LSTM outlier detector on power demand dataset:
![Power demand](fig/power_demand_MC_dropout_results_005.pdf)


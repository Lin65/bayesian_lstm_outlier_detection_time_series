#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 02:54:47 2018

@author: xiaolin
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style("whitegrid")

df = pd.read_csv("resources/data/discords/dutch_power/power_data.txt",header=None,names=['consumption'])

# data time stamp start
dates = pd.date_range('1/1/1997', periods=35040, freq='15Min')

df =df.set_index(dates)

#drop days from 1 to 5 jan to start from a monday.
df = df['1997-01-06':]
#add anomaly column
df['anomaly'] = 0

#a week will have 672 values
step = 672

anomaly_weeks = [12,13,17,18,20,39,51]
validation_weeks = [10,11]
test_weeks = [12,13,14,15,16,17,18,19,20]
train_weeks = [1,2,3,4,5,6,7,8,9]

df_train_list = []
df_valid_list = []
df_test_list = []

lookback = 24
# add lookback data points for stateful lstm

df_train = df[(train_weeks[0]-1)*step: (train_weeks[-1])*step +lookback]
df_validation = df[(validation_weeks[0]-1)*step: (validation_weeks[-1])*step +lookback]
df_test = df[(test_weeks[0]-1)*step: (test_weeks[-1])*step +lookback]

df_dict = {}
df_dict['train'] = df_train
df_dict['test'] = df_test
df_dict['validation'] = df_validation

for key in df_dict:
    np.save("resources/data/discords/dutch_power/%s"%(key),df_dict[key])


count = 0
for key in df_dict:
    print "Size of %s: %d"%(key,df_dict[key].shape[0])
    count += df_dict[key].shape[0]
print "Total: %d"%(count)

for key in df_dict:
    print "Ratio of %s: %f"%(key,float(df_dict[key].shape[0])/count)

for key in df_dict:
    _, ax = plt.subplots()
    df_tmp = df_dict[key]
    df_tmp['consumption'].plot(figsize=(10,3))
    #rows = df_tmp[df_tmp.anomaly.isin([1])].itertuples()
    #for row in rows:
    #    plt.plot(row[0], row[1], 'r.', markersize=5.0)
    #plt.xticks(rotation='vertical')
    plt.title("%s data"%(key))
    plt.show()
    




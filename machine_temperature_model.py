#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 17:43:04 2018

@author: xiaolin
"""

import numpy as np
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding
from keras.layers import LSTM
from keras.datasets import imdb
import keras.backend as K

from keras.callbacks import ModelCheckpoint
from keras.models import Model, load_model, Sequential
from keras.layers import Dense, Activation, Dropout, Input, Masking, TimeDistributed, LSTM, Conv1D
from keras.layers import GRU, Bidirectional, BatchNormalization, Reshape
from keras.optimizers import Adam, SGD
from keras.utils import to_categorical

from scipy.stats import norm

# layers = [ n_units, n_units]
class LSTM_unstateful(object):
    def __init__(self, look_back, layers, dropratio):
        self.look_back = look_back
        if len(layers) > 2:
            raise ValueError('2 layers are enough !')            
        self.layers = layers
        self.dropratio = dropratio
        
    def model(self,input_shape):
        X_input = Input(shape = input_shape)
        if len(self.layers) == 1:
            X = LSTM(units = self.layers[0], dropout = self.dropratio, recurrent_dropout = self.dropratio, return_sequences=False)(X_input)
        else:
            X = LSTM(units = self.layers[0], dropout = self.dropratio, recurrent_dropout = self.dropratio, return_sequences=True)(X_input)
            X = LSTM(units = self.layers[1], dropout = self.dropratio, recurrent_dropout = self.dropratio, return_sequences=False)(X)

        X = Dropout(self.dropratio)(X)
        X = Dense(1, activation='linear')(X)
        model = Model(inputs = X_input, outputs = X)       
        return model
    
    def build_model(self):
        lstm = self.model((self.look_back, 1))
        lstm.compile(loss='mse', optimizer='adam', metrics=['mse'])
        lstm.summary()
        return lstm
       
def train_model(model, X_train, y_train, validation_data, epochs, batch_size, shuffle=True, filepath):
    #filepath = "lstm.h5"
    checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
    callbacks_list = [checkpoint]

    model.fit(X_train, y_train,
         epochs= epochs,
         batch_size= batch_size, shuffle = shuffle, validation_data= validation_data, callbacks=callbacks_list)


def evaluation_model(model, filepath, X_train, y_train, X_validation, y_validation,X_test, y_test):
    model.load_weights(filepath)
    print 'mse of validation: ', model.evaluate(X_validation, y_validation)[0]
    print 'mse of training: ', model.evaluate(X_train, y_train)[0]
    print 'mse of test: ', model.evaluate(X_test, y_test)[0]
    
    
def prediction_point(model, filepath, X_train, X_validation, X_test):
    model.load_weights(filepath)
    pred_test_point = lstm.predict(X_test)
    pred_train_point = lstm.predict(X_train)
    pred_validation_point = lstm.predict(X_validation)
    return pred_train_point, pred_validation_point, pred_test_point
    

def prediction_distribution(model, filepath, X_train, X_validation, X_test, n_smps):
    # MC dropout
    f = K.function([model.layers[0].input, K.learning_phase()], [model.layers[-1].output])
    def predict_with_uncertainty(f, x, n_iter=500):
        result = [0 for i in range(n_iter)]
        for iter in range(n_iter):
            if iter%100 == 0:
                print 'iteration: ', iter
            result[iter] = f((x,1))[0]        
        return result
    
    pred_train = predict_with_uncertainty(f, X_train, n_iter=n_smps)
    pred_test = predict_with_uncertainty(f, X_test, n_iter=n_smps)
    pred_validation = predict_with_uncertainty(f, X_validation, n_iter=n_smps)
    return pred_train, pred_validation, pred_test


    

